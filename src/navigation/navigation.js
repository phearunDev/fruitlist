//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from '../page/Home';
import DetailPage from '../page/DetailPage';

// create a component
export default createStackNavigator ({
    HomeScreen:{
        screen:HomeScreen,
    },
    DetailPage: {
        screen:DetailPage,
    },
  }
  )

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app

