//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
class DetailPage extends Component {

// Config Navigation
    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.fruits}`
    })

    
    constructor(props) {
        super(props);
        this.state = {
            fruits: this.props.navigation.state.params.fruits,
        };
    };


    render() {
        console.log(this.state.fruits)
        return (
            <View style={styles.container}>
                <Text style={styles.detailfruit}>{this.state.fruits}</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0099CC',
    },
    detailfruit:{
        fontSize:25,
        color:"#fff"
    }
});

//make this component available to the app
export default DetailPage;
