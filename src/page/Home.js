//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,Dimensions,TouchableOpacity,FlatList, } from 'react-native';
import Feather from '@expo/vector-icons/Feather';
const { height, width } = Dimensions.get('window')

//array data
const fruits = [
    {fruit: 'Apple'},
    {fruit: 'Banana'},
    {fruit: 'PineApple'},
    {fruit: 'BlackApple'},
    {fruit: 'Grape'},
  ];

// create a component
class HomeScreen extends Component {

// Config Navigation
    static navigationOptions = {
        title: 'FruitList',
        headerBackTitle: null,
      };

      constructor(props) {
        super(props);
        this.state = {
            fruits: fruits,
        }
      }

// shuffle the list
    ShuffleFunction = () => {
        const Shuffle = this.state.fruits.map((a) => [Math.random(), a]).sort((a, b) => a[0] - b[0]).map((a) => a[1])
        this.setState({
            fruits: Shuffle
        })
        console.log(Shuffle)
    }


// Reverse the list 
    ReverseFunction = () => {
        const reversed = this.state.fruits.slice().reverse();
        console.log(reversed)
        this.setState({
            fruits: reversed
        })
    }

//Filter show only fruits whose names start with B
    FilterFunction = (text) => {
        const { fruits } = this.state;
        if (!!text) {
            let filter = fruits.filter((o) => {
                var pattern = new RegExp("\^" + text, 'gi');
                return pattern.test(o.fruit);
            });
            this.setState({ fruits: filter });
        } else {
            this.setState({ fruits: arrays });
        }
    };

     
//Delete fruit from list
    DeleteListFuntion = (item) => {
        const newState = this.state.fruits.slice();
        newState.splice(newState.indexOf(item), 1);

        this.setState({
            fruits: newState
        });
    }
             
// Render Item for Flatlist
      _renderItem(item) {
          this.state
        return (
            
                <TouchableOpacity style={styles.liststyle}
                    // Pass array value to other page 
                    onPress={ () => this.props.navigation.navigate( 'DetailPage', { fruits: item.fruit}) }
                >
                    <View>
                        <Text style={styles.itemstyle}> {item.fruit} </Text>
                    </View>
                    <TouchableOpacity style={{ marginRight:5}} onPress={this.DeleteListFuntion.bind(this, item)}>
                        <Feather name="x" size={40} color='#000'/>
                    </TouchableOpacity>
                </TouchableOpacity>
                
        )
    }




    render() {
        return (
        <View style={styles.container}>
            {/* List of fruits */}
            <View style={{height:400}}>
                <FlatList
                    contentContainerStyle={styles.paragraph}
                    data={this.state.fruits}
                    keyExtractor={(x, i) => i.toString()}
                    renderItem={({ item }) => this._renderItem(item)}
                />
            </View>
            {/* Total fruit count */}
            <View style={styles.wraptotal}>
                    <Text style={styles.totalfont}> TOTAL: {this.state.fruits.length} </Text>
            </View>
            {/* Button at the bottom */}
            <View style={styles.wrapfuncBtn}>
                        <TouchableOpacity style={{marginLeft:20}} onPress={this.ShuffleFunction}>
                            <Text style={styles.funcBtn}> SHUFFLE </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.ReverseFunction}>
                            <Text style={styles.funcBtn}> REVERSE </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{marginRight:20}} onPress={this.FilterFunction}>
                            <Text style={styles.funcBtn}> FILTER </Text>
                        </TouchableOpacity>
                </View>
         </View>
        );
    }
}

// define styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#0099CC',
        
    },
    liststyle: {
        height:50,
        backgroundColor: '#fff',
        marginTop:30,
        marginLeft:30,
        marginRight:30,
        flexDirection: 'row',
        alignContent: 'center',
        justifyContent: 'space-between'
    },
    itemstyle:{
        fontSize:30,
        marginTop:5
    },
    wrapfuncBtn:{
         marginTop:70,      
        alignItems: 'flex-end',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    funcBtn:{
        paddingTop:12,
        paddingLeft:5,
        fontSize:18,
        backgroundColor: '#33b5e5',
        color:"#fff",
        width:105,
        height:50,
        flexBasis: 'auto',
        flexShrink: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
    },
    wraptotal:{
        paddingLeft:25,
        marginTop:15
    },
    totalfont:{
        fontSize:30,
        color:'#fff',
    }
});

//make this component available to the app
export default HomeScreen;
